import java.util.*;

public class Main {


    public static void main(String[] args) {
        int a = 5;

        Integer b = new Integer(4);
        Double c = new Double(4.4);

        Country salis1 = new Country(100, "England", "London");
        Country salis2 = new Country(2, "Germany", "Berlin");
        Country salis3 = new Country(10, "Lithuania", "Vilnius");

        // Saraso tipo kintamasis
        List<Integer> sarasas = new ArrayList<>();

        sarasas.add(4);
        sarasas.add(5);
        sarasas.add(100);
        sarasas.remove(1);


        /* Kolekcijos didziausio nario radimas*/
        System.out.println(Collections.max(sarasas));


        List<Country> visosSalys = new ArrayList<Country>();
        visosSalys.add(salis1);
        visosSalys.add(salis2);
        visosSalys.add(salis3);

        Collections.sort(visosSalys);

        System.out.println(visosSalys);



        for (int i = 0; i < sarasas.size(); i++) {
//            System.out.println(sarasas.get(i));
        }

        for (Integer labas : sarasas) {
            //
//            System.out.println(labas);
        }


        // Set deonstration using HashSet
        Set<String> hash_Set = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        hash_Set.add("Geeks");
        hash_Set.add("For");
        hash_Set.add("Geeks");
        hash_Set.add("Zxample");
        hash_Set.add("Set");




        List<Country> salys = new ArrayList<>();
        salys.add(salis2);
        salys.add(salis1);
        salys.add(salis3);

        /* Array list itteratoriaus panaudojimas */
        Iterator<Country> iteratorArray = salys.iterator();
        while (iteratorArray.hasNext()) {
            Country test = iteratorArray.next();
        }

        for(Country salis: salys) {

        }

        TreeMap<String, Country> capitalCities = new TreeMap<String, Country>();



        capitalCities.put(salis1.name, salis1);

        capitalCities.put(salis2.name, salis2);
        capitalCities.put(salis3.name, salis3);

        for (Map.Entry<String, Country> city : capitalCities.entrySet()) {

//            System.out.println("Key: " + city.getKey() + ". Value: " + city.getValue().getName());

        }


        /* TreeMap  iteratoriaus panaudojimas */
        Iterator iterator = capitalCities.entrySet().iterator();

        while (iterator.hasNext()) {

            Map.Entry salisTest = (Map.Entry)iterator.next();

            System.out.println( salisTest.getValue());

        }

        Country.addCountry(salis1, capitalCities);


        // System.out.println(capitalCities);
//        capitalCities.get("England");

        for (String key : capitalCities.keySet()) {
          //  System.out.println(key);
        }


    }


}

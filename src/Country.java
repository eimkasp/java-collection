import java.util.TreeMap;

public class Country implements Comparable<Country> {

    static class Const {
        static final String zemynai[] = {"Europa", "Azija", "Amerika"};
    }

    @Override
    public int compareTo(Country o) {
        return this.getName().compareTo(o.getName());
    }

    Integer population;
    String name;
    String capital;



    Country(int p, String n, String c) {
        this.population = p;
        this.name = n;
        this.capital = c;
    }

    @Override
    public String toString() {
        return "Country{" +
                "population=" + population +
                ", name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                '}';
    }

    public String getName() {
        return this.name;
    }

    public Integer getPopulation() {
        return this.population;
    }

    public static void addCountry(Country salis, TreeMap<String, Country> capitalCities) {

        if (capitalCities.get(salis.name) == null) {
            capitalCities.put(salis.name, salis);
        } else {
            for(int i = 1; i < 10; i++) {
                if(capitalCities.get(salis.name + "_" + i) == null) {
                    capitalCities.put(salis.name + "_" + i, salis);
                    break;
                }
            }
        }
    }
}
